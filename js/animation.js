'use strict';
~ function() {
    var fullHeight;

    window.init = function() {

        reload.onclick = reloadBanners;

        setXYData();

        function setXYData() {

            for (var i = 0; i < bannerContainer.children.length; i++) {
                var xPos = $(bannerContainer.children[i]).css('-webkit-transform').split(/[(,)]/)[5];
                var yPos = $(bannerContainer.children[i]).css('-webkit-transform').split(/[(,)]/)[6]
                bannerContainer.children[i].setAttribute('data-x', xPos);
                bannerContainer.children[i].setAttribute('data-y', yPos);

            }

        }

        function reloadBanners() {

            $('.draggable').each(function() {
                var link = $(this).children('iframe').attr('src');
                $(this).children('iframe').attr('src', '');
                $(this).children('iframe').attr('src', link);
            });

            if (bannerContainer.hasChildNodes()) {
                //
            }
        }

        $(".reloadBanner").on('click', function(event) {
            var iframe = $(this).parent().siblings().attr('id');
            var iframeId = document.getElementById(iframe);
            var iframeSrc = $(iframeId).attr('src');
            iframeSrc = $(iframeId).attr('src', iframeSrc);
        });

        $(".minimiseBanner").on("click", function() {
            var iframeContainer = $(this).parent().parent().attr('id');
            var iframe = $(this).parent().siblings().attr('id');
            var iframeId = document.getElementById(iframe);
            var iframeContainerId = document.getElementById(iframeContainer);

            if ($(iframeId).height() > 30) {
                fullHeight = $(iframeContainerId).height();
                $(iframeId).animate({ height: "0px" }, 100);
            } else if ($(iframeId).height() == 0) {
                fullHeight = $(iframeContainerId).height();
                $(iframeId).animate({ height: (fullHeight - 30) + "px" }, 100);
            }

            // document.title = $(this).parent().siblings().contentWindow.document.title

        });

        $(".closeBanner").on('click', function(event) {
            var iframeContainer = $(this).parent().parent().attr('id');
            var iframeContainerId = document.getElementById(iframeContainer);
            var iframe = $(this).parent().siblings().attr('id');
            var iframeId = document.getElementById(iframe);

            // $(iframeId).remove();
            $(iframeContainerId).css("display", "none");

            var d = document.createElement('div');
            var text = $(iframeId).width() + "x" + $(iframeId).height();

            $(d).addClass('sizeBtn')
                .html(text)
                .appendTo($("#multipleFileHeader")) //main div
                .click(function() {
                    $(iframeContainerId).css("display", "block");
                    $(this).remove();
                })

        });

        $(".bannerHeader").on('mouseover', function(event) {
            var iframe = $(this).siblings().attr('id');
            var iframeId = document.getElementById(iframe);
            //console.log(iframe);

            var iframeTitle = iframeId.contentDocument.title;

            var d1 = document.createElement('span');

            $(d1).addClass('tooltiptext')
                .html(iframeTitle)
                .appendTo($(this))
                .css('visibility', 'visible')
                .css('top', '-28px')
                .css('width', '250px')
                .css('opacity', '0.8')
                .delay(1500)
                .queue(function() {
                    $(this).remove();
                });
            // .click(function() {
            //     $(iframeContainerId).css("display", "block");
            //     $(this).remove();
            // })
        });

        $(".toggleButton").click(function() {
            $(".on").fadeToggle();
            $(".off").fadeToggle();
            $(".handle").toggleClass("moveHandleLeft", 500);
            $(".bannerHeader").toggleClass("hideBannerHeader", 500);

        });

        // console.log(document.title);


        /* -------------------------------- 
                    target elements with the "draggable" class
----------------------------------------------- */
        interact('.draggable')
            .draggable({
                // enable inertial throwing
                inertia: true,
                // keep the element within the area of it's parent
                restrict: {
                    restriction: "parent",
                    endOnly: true,
                    elementRect: {
                        top: 0,
                        left: 0,
                        bottom: 1,
                        right: 1
                    }
                },
                // enable autoScroll
                autoScroll: true,

                // call this function on every dragmove event
                onmove: dragMoveListener,
                // call this function on every dragend event
                onend: function(event) {
                    var textEl = event.target.querySelector('p');

                    textEl && (textEl.textContent =
                        'moved a distance of ' + (Math.sqrt(event.dx * event.dx +
                            event.dy * event.dy) | 0) + 'px');
                }
            });

        function dragMoveListener(event) {
            var target = event.target,
                // keep the dragged position in the data-x/data-y attributes
                x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

            // translate the element
            target.style.webkitTransform =
                target.style.transform =
                'translate(' + x + 'px, ' + y + 'px)';

            // update the posiion attributes
            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);
        }

        // this is used later in the resizing and gesture demos
        window.dragMoveListener = dragMoveListener;
    }


}();